
const PID_VEHICLE_SPEED = "0D"
const PID_MAF = "10"

export function newTripStatistic(startLocation: any, instantConsumptionLPS: number, tripRecord: any, previousTime: number) {
    const timeDifferenceS = (tripRecord.time - previousTime) / 1000; // [ms]
    const fuelConsumptionL = instantConsumptionLPS * timeDifferenceS; // calculate consumption [L]
    return new TripStatistics(startLocation, tripRecord.location, fuelConsumptionL, tripRecord.time);
}

export function getTripStatistic(previous: TripStatistics, instantConsumptionLPS: number, tripRecord: any) {
    return newTripStatistic(previous.locationEnd, instantConsumptionLPS, tripRecord, previous.time)
}

export function updateTripStatistic(previous: TripStatistics, instantConsumptionLPS: number, tripRecord: any) {
    const current = getTripStatistic(previous, instantConsumptionLPS, tripRecord);
    return new TripStatistics(previous.locationStart, current.locationEnd, previous.consumption + current.consumption, tripRecord.time);
}

export function getFuelConsumptionByMaf(obdDataArray: ObdData[]) {
    const airFuelRatio = 1 / 14.7 // [g fuel / g air] for gasoline (TODO: get by fuel type)
    const fuelDensityRatio = 1 / 710 // 1L = 710g  (TODO: get by fuel type)
    const massAirFlowGPS = getMafValue(obdDataArray); // [g/s] of air
    return massAirFlowGPS * airFuelRatio * fuelDensityRatio
}

export function getSpeedValue(obdDataArray: ObdData[]): number {
    const rawString = obdDataArray.find(data => data.pid === PID_VEHICLE_SPEED)!.raw!;
    const rawData = parseHexString(rawString);
    return rawData[2];
}

export function getMafValue(obdDataArray: ObdData[]): number {
    const rawString = obdDataArray.find(data => data.pid === PID_MAF)!.raw!;
    const rawData = parseHexString(rawString);
    return (rawData[2] * 256.0 + rawData[3]) / 100.0;
}

export function canUseMafVssMethod(obdDataArray: ObdData[]): boolean {
    return hasPid(obdDataArray, PID_MAF) && hasPid(obdDataArray, PID_VEHICLE_SPEED);
}

function hasPid(obdDataArray: ObdData[], pid: String): boolean {
    return obdDataArray.some(withPid(pid));
}

function withPid(pid: String): (data: ObdData) => boolean {
    return data => data.pid === pid;
}

function parseHexString(hex: String): number[] {
    const result = [];
    let begin = 0;
    let end = 2;
    while (end <= hex.length) {
        result.push(parseInt(hex.substring(begin, end), 16));
        begin = end;
        end += 2;
    }
    return result;
}

export class ObdData {
    pid: string | undefined;
    raw: string | undefined;
}

export class TripStatistics {

    locationStart: any;
    locationEnd: any;
    consumption: number; // [L]
    time: number; // [ms]

    constructor(locationStart: any, locationEnd: any, consumption: number, time: number) {
        this.locationStart = locationStart;
        this.locationEnd = locationEnd;
        this.consumption = consumption;
        this.time = time;
    }
}
