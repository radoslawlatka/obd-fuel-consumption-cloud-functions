import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as obd from './obd';


admin.initializeApp()

export const chartData = functions.https.onRequest(async (request, response) => {
    const db = admin.database()
    const tripId = request.query.tripId

    try {
        const statisticsRef = db.ref(`/tripStatistics/${tripId}/`)
        const statisticsSnap = (await statisticsRef.orderByKey().once('value'))
        const chartDataRecords: any[] = []
        let distance = 0
        let consumption = 0
        statisticsSnap.forEach(statisticSnap => {
            const stat = statisticSnap.val()
            consumption += stat.consumption
            distance += getDistance(stat.locationStart, stat.locationEnd)
            chartDataRecords.push(`${distance};${consumption}`)
        })

        response.json(chartDataRecords)
    } catch (error) {
        console.error(error)
        response.json(`error: ${error}`)
    }

})

export const calculateConsumption = functions.https.onRequest(async (request, response) => {
    const tripId = request.query.tripId
    try {
        await admin.database().ref(`/tripStatistics/${tripId}/`).set(null)
        await admin.database().ref(`/tripummary/${tripId}/`).set(null)
        await calculateConsumptionForTrip(tripId)
        response.json("success!")
    } catch (error) {
        console.error(error)
        response.json(`error: ${error}`)
    }

})


export const onTripFinish = functions.database
    .ref(`/tripInfo/{tripId}/status`)
    .onWrite(async (change, context) => {
        if (change.after.val() !== 'FINISHED') return

        const tripId = context.params.tripId
        console.info(`trip: ${tripId} finished, start calculate data...`)

        try {
            await calculateConsumptionForTrip(tripId)
        } catch (error) {
            console.error(`Error occured: ${error}`)
        }
    })

async function calculateConsumptionForTrip(tripId: string) {
    const db = admin.database()

    const statisticsRef = db.ref(`/tripStatistics/${tripId}/`)
    const tripStartTime = (await db.ref(`/tripInfo/${tripId}/started`).once('value')).val()
    const tripRecordsSnap = (await db.ref(`/trips/${tripId}/records/`).orderByKey().once('value'))
    const tripRecords: any[] = []
    tripRecordsSnap.forEach(tripRecordSnap => {
        tripRecords.push(tripRecordSnap.val())
        console.info(`computing record: ${tripRecordSnap.toJSON()}`)
    })
    let previousStatistic: obd.TripStatistics
    const results = new Array<obd.TripStatistics>()

    console.info(`trip records count: ${tripRecords.length}, start calculate consumption...`)


    let mafSum = 0
    let mafCount = 0

    tripRecords.forEach(record => {
        const obdDataArray: [] = record.obd_data

        if (obd.canUseMafVssMethod(obdDataArray)) {
            const speedKmPH = obd.getSpeedValue(obdDataArray) // [km/h]
            console.debug(`speed: ${speedKmPH} [km/h]`)
            const instantConsumptionLPS = obd.getFuelConsumptionByMaf(obdDataArray)
            console.debug(`instant consumption: ${instantConsumptionLPS} [L/s]`)

            if (previousStatistic) {
                // at least one statistic defined
                console.debug(`last statistic found`)
                if (speedKmPH > 0) {
                    console.debug(`speed >0, create new statistic record`)
                    const statistic = obd.getTripStatistic(previousStatistic, instantConsumptionLPS, record)
                    results.push(statistic)
                    previousStatistic = statistic
                } else {
                    console.info(`speed == 0, update last statistic`)
                    const statistic = obd.updateTripStatistic(previousStatistic, instantConsumptionLPS, record)
                    // replace last
                    results.pop()
                    results.push(statistic)
                    previousStatistic = statistic
                }
            } else {
                // first statistic item
                console.debug(`start trip time: ${tripStartTime}`)
                const statistic = obd.newTripStatistic(record.location, instantConsumptionLPS, record, tripStartTime)
                results.push(statistic)
                previousStatistic = statistic
            }

            mafSum += obd.getMafValue(obdDataArray)
            mafCount += 1

        } else {
            console.warn(`missing data for maf-vss method`)
        }
    })

    // calculate summary:
    let totalDistance = 0 // [km]
    let totalTime = 0

    results.forEach(statistic => {
        totalDistance += getDistance(statistic.locationStart, statistic.locationEnd)
    })

    if (results.length > 1) {
        totalTime = results[results.length - 1].time - results[0].time
    }

    const totalTimeS = totalTime / 1000
    const fuelDensityRatio = 1 / 710 // 1L = 710g
    const mafAvg = mafSum / mafCount
    const totalFuelConsumptionLPS = mafAvg / 14.7 * fuelDensityRatio // [L/s]
    const totalConsumptionL = totalFuelConsumptionLPS * totalTimeS // [L]
    const totalConsumptionLP100Km = 100 * totalConsumptionL / totalDistance // [L/100km]

    // update trip info
    const promises = new Array()
    const updatePromise = db.ref(`/tripSummary/${tripId}/`).update({
        distance: totalDistance,
        time: totalTime,
        avgFuelConsumptionLPS: totalFuelConsumptionLPS,
        avgConsumptionLP100Km: totalConsumptionLP100Km,
        totalConsumptionL: totalConsumptionL,
        mafAvg: mafAvg
    })
    promises.push(updatePromise)

    console.log('consumption calculated, saving in database...')

    // save all statistics
    results.forEach(result => {
        promises.push(statisticsRef.push(result))
    })

    return Promise.all(promises)
        .then(snap => { console.info('data saved!') })
        .catch(error => { console.error(`save data error: ${error.stack}`) })
}

function getDistance(start: any, end: any) {
    const p: number = 0.017453292519943295 // Math.PI / 180
    const a = 0.5 - Math.cos((end.lat - start.lat) * p) / 2 + Math.cos(start.lat * p) * Math.cos(end.lat * p) * (1 - Math.cos((end.lon - start.lon) * p)) / 2
    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

